from sense_hat import SenseHat
import web
import json

sense = SenseHat()

urls = (
	'/', 'index',
	'/temp', 'temp',
	'/humidity', 'humidity',
	'/show', 'show'	
)

class index:
    def GET(self):
        return "Raspeberry Pi"
        
class temp:
	def GET(self):
		result = { 'temperature': sense.get_temperature_from_pressure() }
		web.header( 'Content-Type', 'application/json' )
		return json.dumps( result )
        
class humidity:
	def GET(self):
		result = { 'humidity': sense.humidity }
		web.header( 'Content-Type', 'application/json' )
		return json.dumps( result )

class show:
	def POST(self):
		data = web.input()
		sense.show_message(data.message)


if __name__ == "__main__": 
    app = web.application(urls, globals())
    app.run()
